import { Alert, Button, Snackbar } from "@mui/material";
import { GoogleMap, LoadScript, Polygon, Marker } from "@react-google-maps/api";
import React, { useCallback, useEffect, useRef, useState } from "react";
import styles from '../../styles/Edit.module.css';
import { Router, useRouter } from 'next/router'



const colors = ['green', 'blue', 'yellow', 'black', 'red', 'pink', 'violet', 'grey', 'white', 'brown']

function EditPolygon(props) {
  // Store Polygon path in state
  const router = useRouter()

  const siteIdA = router.query.siteId
  const [path, setPath] = useState([
  ]);


  const [siteData, setSiteData] = useState({})

  const [allSitesData, setAllSitesData] = useState([])
  // Define refs for Polygon instance and listeners
  const polygonRef = useRef(null);
  const listenersRef = useRef([]);

  // Call setPath with new edited path
  const onEdit = useCallback(() => {
    if (polygonRef.current) {
      const nextPath = polygonRef.current
        .getPath()
        .getArray()
        .map(latLng => {
          return { lat: latLng.lat(), lng: latLng.lng() };
        });
      setPath(nextPath);
    }
  }, [setPath]);

  // Bind refs to current Polygon and listeners
  const onLoad = useCallback(
    polygon => {
      polygonRef.current = polygon;
      const path = polygon.getPath();
      listenersRef.current.push(
        path.addListener("set_at", onEdit),
        path.addListener("insert_at", onEdit),
        path.addListener("remove_at", onEdit)
      );
    },
    [onEdit]
  );

  // Clean up refs
  const onUnmount = useCallback(() => {
    listenersRef.current.forEach(lis => lis.remove());
    polygonRef.current = null;
  }, []);


  useEffect(async () => {
    const data = await fetch(`http://localhost:3001/api/get-site-data/${siteIdA}`).then(x => x.json())
    setSiteData(data)


    const allSitesD = await fetch(`http://localhost:3001/api/get-sites`).then(x => x.json())
    const allSitesData = (allSitesD || []).filter((site) => {

      return site.siteid !== Number(siteIdA)
    })

    setAllSitesData(allSitesData)
    setPath(data.polygon?.length ? data.polygon.map((x) => ({ lat: Number(x[0]), lng: Number(x[1]) })) : [{ lat: data.location.x, lng: data.location.y }, { lat: data.location.x, lng: data.location.y }, { lat: data.location.x, lng: data.location.y }])
  }, [siteIdA])

  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event) => {
    setOpen(false);
  };


  return (
    <div className="App">
      <div className={styles.flexHead}>
        <div>
          SiteId : {siteData.siteid}
        </div>
        <div>
          Site Name : {siteData.name}
        </div>
        <div>


          <Button variant="contained" onClick={async () => {
            const apiResult = await fetch(`http://localhost:3001/api/store-site-polygon`, { method: 'POST', body: JSON.stringify({ siteid: siteIdA, polygon: path.map(pt => ([pt.lat, pt.lng])) }), headers: { 'Content-type': 'application/json' } })
            if (apiResult) {
              handleClick()
              setTimeout(() => { router.push("/") }, 1500)
            }
          }}>Save</Button>
        </div>
      </div>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
        <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
          Data Saved Successfuly
        </Alert>
      </Snackbar>
      <LoadScript
        id="script-loader"
        googleMapsApiKey="AIzaSyCu4uyf9ln--tU-8V32nnFyfk8GN4koLI0"
        language="en"
        region="in"
      >
        <GoogleMap
          mapContainerClassName="App-map"
          center={{ lat: siteData?.location?.x, lng: siteData?.location?.y }}
          zoom={12}
          version="weekly"
          on
        >
          <Marker position={{ lat: siteData?.location?.x, lng: siteData?.location?.y }} icon={{ url: "/apollo_logo.png" }} />


          {allSitesData.map(((siteData) => (<>

            <Polygon
              options={{ fillOpacity: 0.01, strokeColor: 'green', strokeOpacity: 0.6 }}
              path={siteData.polygon.map((poly) => ({ lat: Number(poly[0]), lng: Number(poly[1]) }))}
            />
          </>
          )))}
          <Polygon
            // Make the Polygon editable / draggable
            editable
            draggable
            options={{ fillColor: 'orange', fillOpacity: 0.8 }}
            path={path}
            // Event used when manipulating and adding points
            onMouseUp={onEdit}
            // Event used when dragging the whole Polygon
            onDragEnd={onEdit}
            onLoad={onLoad}
            onUnmount={onUnmount}
          />
        </GoogleMap>
      </LoadScript>
    </div>
  );
}

export default EditPolygon