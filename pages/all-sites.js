import { DialogActions } from "@material-ui/core";
import CloseIcon from '@mui/icons-material/Close';
import { Button, DialogContent, IconButton, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import { color } from "@mui/system";
import { GoogleMap, LoadScript, Marker, Polygon } from "@react-google-maps/api";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { useRouter } from 'next/router'


const colors = ['green', 'blue', 'yellow', 'black', 'red', 'pink', 'violet', 'grey', 'white', 'brown']

// This example presents a way to handle editing a Polygon
// The objective is to get the new path on every editing event :
// - on dragging the whole Polygon
// - on moving one of the existing points (vertex)
// - on adding a new point by dragging an edge point (midway between two vertices)

// We achieve it by defining refs for the google maps API Polygon instances and listeners with `useRef`
// Then we bind those refs to the currents instances with the help of `onLoad`
// Then we get the new path value with the `onEdit` `useCallback` and pass it to `setPath`
// Finally we clean up the refs with `onUnmount`


function AllSites() {
  // Store Polygon path in state
  const [siteData, setSiteData] = React.useState([]);

  const [allSitesData, setAllSitesData] = useState([])

  const [open, setOpen] = useState(false)
  const router = useRouter()
  const handleClickOpen = async (opt) => {
    const [lat, lng] = [opt.latLng.lat(), opt.latLng.lng()]
    console.log(lat, lng)
    const data = await fetch(`http://localhost:3001/api/check-is-inside?lat=${lat}&lng=${lng}`).then(x => x.json())
    setSiteData(data)
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
      padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));


  // Define refs for Polygon instance and listeners
  const polygonRef = useRef(null);
  const listenersRef = useRef([]);

  const onUnmount = useCallback(() => {
    listenersRef.current.forEach(lis => lis.remove());
    polygonRef.current = null;
  }, []);


  useEffect(async () => {
    const data = await fetch(`http://localhost:3001/api/get-sites`).then(x => x.json())

    setAllSitesData(data)
  }, [])

  return (
    <div className="App">
      <LoadScript
        id="script-loader"
        googleMapsApiKey="AIzaSyCu4uyf9ln--tU-8V32nnFyfk8GN4koLI0"
        language="en"
        region="in"
      >
        <GoogleMap
          options={{ styles: [{ featureType: 'poi', stylers: [{ visibility: 'off' }] }] }}
          mapContainerClassName="App-map"
          center={allSitesData.length ? { lat: allSitesData[0].location.x, lng: allSitesData[0].location.y } : { lat: 0, lng: 0 }}
          zoom={6}
          version="weekly"
          on
          onClick={(opt) => handleClickOpen(opt)}
        >
          {allSitesData.map(((siteData) => (<>
            <Marker position={{ lat: siteData?.location?.x, lng: siteData?.location?.y }} icon={{ url: "/apollo_logo.png" }} />

            <Polygon
              onClick={(opt) => { handleClickOpen(opt); }}
              options={{ fillColor: colors[Math.floor(Math.random() * colors.length)], strokeColor: colors[Math.floor(Math.random() * colors.length)] }}
              path={siteData.polygon.map((poly) => ({ lat: Number(poly[0]), lng: Number(poly[1]) }))}
            />
          </>
          )))}

        </GoogleMap>
      </LoadScript>
      <div className="goHome">
        <Button variant="contained" onClick={() => router.push("/")}>Go Home</Button>
      </div>
      <div>

        <BootstrapDialog
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={open}
        >
          <DialogTitle sx={{ m: 0, p: 2 }}>
            {siteData.length} Sites Available
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: 'absolute',
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <CloseIcon />
            </IconButton>
          </DialogTitle>
          <DialogContent dividers>
            <TableContainer component={Paper}>
              <Table aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Unique Id</TableCell>
                    <TableCell>Site Name</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {siteData.map((row) => (
                    <TableRow
                      key={row.siteid}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell component="th" scope="row">
                        {row.siteid}
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {row.name}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </DialogContent>

        </BootstrapDialog>
      </div>

    </div >

  );
}

export default AllSites