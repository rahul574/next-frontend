import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import React, { useEffect, useState } from 'react';
import styles from '../styles/Home.module.css';
import fetch from 'node-fetch';
import EditIcon from '@mui/icons-material/Edit';
import Router from "next/router";
import Button from '@mui/material/Button'
import { useRouter } from 'next/router'
import { Dialog, DialogActions, DialogContent, DialogTitle, TextField } from '@material-ui/core';



const AddNewStoreDialog = ({ open, handleClose, newStoreDetails, setNewStoreDetails, router }) => (
  <div>
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>Add New Store</DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label="Site Name"
          type="text"
          fullWidth
          variant="standard"
          value={newStoreDetails.name}
          onChange={(evt) => { setNewStoreDetails({ ...newStoreDetails, name: evt.target.value }) }}
        />
        <TextField
          autoFocus
          margin="dense"
          id="lat"
          label="Latitude"
          type="number"
          fullWidth
          variant="standard"
          value={newStoreDetails.lat}
          onChange={(evt) => { setNewStoreDetails({ ...newStoreDetails, lat: evt.target.value }) }}
        />
        <TextField
          autoFocus
          margin="dense"
          id="lng"
          label="Longitude"
          type="number"
          fullWidth
          variant="standard"
          value={newStoreDetails.lng}
          onChange={(evt) => { setNewStoreDetails({ ...newStoreDetails, lng: evt.target.value }) }}
        />
        <TextField
          autoFocus
          margin="dense"
          id="distance"
          label="Distance"
          type="number"
          fullWidth
          variant="standard"
          value={newStoreDetails.distance}
          onChange={(evt) => { setNewStoreDetails({ ...newStoreDetails, distance: evt.target.value }) }}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={async () => {
          const apiResult = await fetch(`http://localhost:3001/api/create-site-polygon?lat=${newStoreDetails.lat}&lng=${newStoreDetails.lng}&distance=${newStoreDetails.distance}&name=${newStoreDetails.name}`, { method: 'GET', headers: { 'Content-type': 'application/json' } })
          router.reload()
        }}>Add New Store</Button>
      </DialogActions>
    </Dialog>
  </div>
);

export default function Home() {

  const [sitesData, setSitesData] = useState([])
  const router = useRouter()

  const [open, setOpen] = React.useState(false);

  const [newStoreDetails, setNewStoreDetails] = React.useState({ name: '', lat: '', lng: '', distance: '' })

  const handleClickOpen = () => { setOpen(true) }

  const handleClose = () => {
    setOpen(false);
  };


  useEffect(async () => {
    const data = await fetch(`http://localhost:3001/api/get-sites`).then(x => x.json())

    setSitesData(data)
  }, [])
  return (
    <div className={styles.container}>
      <AddNewStoreDialog open={open} handleClose={handleClose} newStoreDetails={newStoreDetails} setNewStoreDetails={setNewStoreDetails} router={router} />
      <Button variant="contained" onClick={() => { router.push("/all-sites") }} className={styles.allsiteButton}>Check All Sites</Button>
      <Button variant="contained" onClick={() => { setOpen(true) }} className={styles.allsiteButton}>Add New Store</Button>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow className={styles.RowHead}>
              <TableCell>Unique Id</TableCell>
              <TableCell>Site Name</TableCell>
              <TableCell>Location</TableCell>
              <TableCell>Change Servicabe Area</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {sitesData.map((siteData) => (
              < TableRow key={siteData.siteid} >
                <TableCell>{siteData.siteid}</TableCell>
                <TableCell>{siteData.name}</TableCell>
                <TableCell>{`${siteData.location.x} , ${siteData.location.y}`}</TableCell>
                <TableCell>
                  <div className={styles.editButton} onClick={() => (
                    Router.push(`/edit-polygon/${siteData.siteid}`)
                  )}>
                    <EditIcon>
                    </EditIcon>
                  </div>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div >
  )
}
